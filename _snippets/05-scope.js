
var c: MyClass = new Myclas()

/**
 * In JavaScript there are two types of scope:
 * * Local scope
 * * Global scope
 * 
 * JavaScript has function scope: Each function creates a new scope.
 * Scope determines the accessibility (visibility) of these variables.
 * Variables defined inside a function are not accessible (visible) from outside the function.
 */

'use strict'
var global
var otherGlobal = function () { }
var a = 10;
function globalFunction() {
    var a = 1; // local
    test = 2 // global // error in 'strict mode'
    var global; // local
    return function localFunction(a) { // local function to this function
        a = 2; // local to this function
        return a;
    }
    console.log(localFunction()) // 2
}

globalFunction.localFunction(a);





/**
 * Scope dilemma and Closures
 */

// Initiate counter
var counter = 0; // global ... bad!

// Function to increment counter
function add() {
    // var counter = 0;
    counter += 1;
}


/**
 *  Closure by an IIFEE
 * */
var add = (function () {
    var counter = 0;
    return function add() {
        counter += 1;
        console.log(counter);
    }
})();

add(); // 1
add(); // 2
add(); // 3


/**
 * Closure factory
 * The Todo factory is a closure. 
 * It builds immutable objects. 
 * It can be used with or without new. 
 * The prototype is created only once and it is private.
 */
function createTodoFactory() {
    function Todo(spec) {
        Object.assign(this, spec);
    }

    return function (spec) {
        let todo = new Todo(spec);
        return Object.freeze(todo);
    }
}

let Todo = (function() {
    function Todo(spec) {
        Object.assign(this, spec);
    }

    return function (spec) {
        let todo = new Todo(spec);
        return Object.freeze(todo);
    }
})();

