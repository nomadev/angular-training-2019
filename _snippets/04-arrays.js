

var fruits = ["Apple", "Banana"];
var persons = [
    {
        id: 1,
        name: 'Claudio'
    },
    {
        id: 2,
        name: 'John'
    },
]
var numbers = [ 1, 2, 3, 4];

fruits.length // 2
fruits[0] // Apple



Array.isArray(fruits) // Returns true if a variable is an array, if not false.
Array.of('ciao', 2, {}) // Creates a new Array instance with a variable number of arguments

// Array prototypal methos
arr.push(item) // adds item to the end,
arr.push(...item) // adds items to the end, (... is ES6)
arr.pop() // extracts and returns an item from the end,
arr.shift() // extracts  and returns an item from the beginning,
arr.unshift(...items) // adds items to the beginning.

fruits.indexOf("Banana"); // @return > -1 if string matches. the number rappresents the index

fruits.findIndex(function(f) { return f === "Banana"}); // 0

persons.find(function(p) { return p.id === 2}); // { id: 2, name: 'John' }

fruits.findIndex(function(f) { return f === "Banana"}); // 0

// creates a new array with the results of calling a provided function on every element in the calling array.
numbers.map(x => x * 2); // [2, 8, 18, 32]

numbers.map(x => [x * 2]);  // [[2], [4], [6], [8]]

// flatmap first maps each element using a mapping function, then flattens the result into a new array
numbers.flatMap(x => [x * 2]); // [2, 4, 6, 8]

// only one level is flattened
numbers.flatMap(x => [[x * 2]]); // [[2], [4], [6], [8]]

// Adds and/or removes elements from an array.
fruits.splice(0, fruits.length, 'Kiwi')  // Kiwi


// Complete list
// @https://developer.mozilla.org/it/docs/Web/JavaScript/Reference/Global_Objects/Array


