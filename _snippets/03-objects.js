// In JavaScript, almost "everything" is an object.
// All JavaScript values, except primitives, are objects.

var person = {
    name: 'Jim',
    age: 70,
}
person['name'] = 'Ros';
person.name = 'Adam';

// Vanilla JS
Object.keys(person).map(function (key, index) {
    var value = person[key];
    console.log(key, value);
});

// ES 6
for (key in person) {
    console.log(key, person[key]);
}


// La proprietà Object.prototype rappresenta l'oggetto prototipo di Object.
// OLOO (Object Linked To Other Object) è un paradigma che sfrutta la proprietà prototype
var Persona = function (nome) {
    this.canSpeak = true;
    this.nome = nome;
};

Persona.prototype.saluta = function () {
    console.log('Ciao, mi chiamo ' + this.nome);
};
var p1 = new Persona('claudio');
p1.saluta(); // Ciao mi chiamo claudio




// prototypal inherance
var Dipendente = function (nome, titolo) {
    Persona.call(this, nome);
    this.titolo = titolo;
};
Dipendente.prototype.saluta = function () {
    if (this.canSpeak)
        console.log('Ciao mi chiamo ' + this.nome + ' e lavoro come ', this.titolo);
}
var d1 = new Dipendente('claudio', 'consulente');
d1.saluta();  // Ciao mi chiamo claudio e lavoro come consulente





// composing
var Dipendente2 = function (nome, titolo) {
    this.persona = new Persona(nome);
    this.titolo = titolo;
    this.persona.saluta = function () {
        console.log('Ciao mi chiamo ' + this.nome + ' e lavoro come ', this.titolo);
    }.bind(this)
};
var d2 = new Dipendente2('claudio', 'consulente');
d2.persona.saluta(); // Ciao mi chiamo claudio e lavoro come consulente


/**
 * Il metodo hasOwnProperty() restituisce un boolean che indica se l'oggetto ha o meno 
 * la proprieta' specificata.
 */
d2.hasOwnProperty('nome') // false
d2.hasOwnProperty('titolo') // true

d2 instanceof Dipendente2 // true
d1 instanceof Persona // false





/**
 * Il metodo Object.create() crea un nuovo oggetto a partire dall'oggetto prototipo e 
 * dalle proprietà specificate.
 */
// Shape - superclass
function Shape() {
    this.x = 0;
    this.y = 0;
}

// superclass method
Shape.prototype.move = function (x, y) {
    this.x += x;
    this.y += y;
    console.info('Shape moved.');
};

// Rectangle - subclass
function Rectangle(width, height) {
    this.width = width, this.height = height;
    this.area = function() { 
        return this.width * this.height
    };
    Shape.call(this); // call super constructor.
}

// subclass extends superclass
Rectangle.prototype = Object.create(Shape.prototype);
Rectangle.prototype.constructor = Rectangle;

var rect = new Rectangle(10, 20);
rect.move(1, 1); // Outputs, 'Shape moved.'
rect.area(); // 200

rect instanceof Rectangle // true
rect instanceof Shape // true

rect.hasOwnProperty('move') // false
rect.hasOwnProperty('width') // true
rect.hasOwnProperty('area') // true

