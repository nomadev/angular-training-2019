import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 | exponentialStrength:10 }}
 *   formats to: 1024
 * 
 * https://angular.io/generated/live-examples/pipes/stackblitz.html
*/
@Pipe({ name: 'exponentialStrength' })
export class ExponentialStrengthPipe implements PipeTransform {
    transform(value: number, exponent: string): number {
        const exp = parseFloat(exponent);
        return Math.pow(value, isNaN(exp) ? 1 : exp);
    }
}

/**
 * This pipe definition reveals the following key points:

    A pipe is a class decorated with pipe metadata.

    The pipe class implements the PipeTransform interface's transform method
    that accepts an input value followed by optional parameters and returns the transformed value.

    There will be one additional argument to the transform method for each parameter passed to the pipe.
    Your pipe has one such parameter: the exponent.

    To tell Angular that this is a pipe, you apply the @Pipe decorator,
    which you import from the core Angular library.

    The @Pipe decorator allows you to define the pipe name that you'll use within template expressions.
    It must be a valid JavaScript identifier. Your pipe's name is exponentialStrength.
 */