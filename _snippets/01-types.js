// Primitive data types are:
string
number
boolean
null
undefined
/**
 * Reference data types
 * (Arrays, Objects) are data structures. These data structures hold the information to manipulate our application.
 */
Objects
Arrays

// ES 6
Symbol




typeof []; // object
typeof {}; // object
typeof new Date() // object
typeof ''; // string
typeof 1; // number
typeof function () {}; // function
typeof /test/i; // object
typeof true; // boolean
typeof null; // object
typeof undefined; // undefined


// Todd's trick
// You pass the thing you want to check the type of into call() as an argument. The gets its prototype and converts it to a string
Object.prototype.toString.call([]); // [object Array]
Object.prototype.toString.call({}); // [object Object]
Object.prototype.toString.call(''); // [object String]
Object.prototype.toString.call(new Date()); // [object Date]
Object.prototype.toString.call(1); // [object Number]
Object.prototype.toString.call(function () {}); // [object Function]
Object.prototype.toString.call(/test/i); // [object RegExp]
Object.prototype.toString.call(true); // [object Boolean]
Object.prototype.toString.call(null); // [object Null]
Object.prototype.toString.call(); // [object Undefined]

function checkType(obj) {
    return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
}




var simpleStr = 'This is a simple string'; 
var myString  = new String();
var newStr    = new String('String created with constructor');
var myDate    = new Date();
var myObj     = {};

simpleStr instanceof String; // returns false, checks the prototype chain, finds undefined
myString  instanceof String; // returns true
newStr    instanceof String; // returns true
myString  instanceof Object; // returns true

myObj    instanceof Object;    // returns true, despite an undefined prototype
({})     instanceof Object;    // returns true, same case as above

myString instanceof Date;   // returns false

myDate instanceof Date;     // returns true
myDate instanceof Object;   // returns true
myDate instanceof String;   // returns false