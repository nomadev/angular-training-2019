/**
 * In JavaScript all functions are object methods.
 * If a function is not a method of a JavaScript object, it is a function of the global object.
 * The example below creates an object with 3 properties, firstName, lastName, fullName.
 */

function myFunction(a, b) {
    return a * b;
}
myFunction(10, 2); // Will return 20

// in a browser
window.myFunction(10, 2); // Will also return 20
// in Node.js
global.myFunction(10, 2); // Will also return 20






/**
The this Keyword
In a function definition, this refers to the "owner" of the function.
In the example above, this is the person object that "owns" the fullName function.
In other words, this.firstName means the firstName property of this object.
 */
var person = {
    firstName: "John",
    lastName: "Doe",
    fullName: function () {
        console.log(this.firstName + " " + this.lastName);
    }
}
setTimeout(person.fullName, 100);   // Will print "John Doe"









var person = {
    firstName: "John",
    lastName: "Doe",
    fullName: function () {
        setTimeout(function () {
            console.log(this.firstName + " " + this.lastName);
        }, 1000)
    }
}
person.fullName();   // Will return "undefined undefined"


/**
 * Il metodo bind() crea una nuova funzione che, quando chiamata, ha parola chiave this impostata sul valore fornito, 
 * con una data sequenza di argomenti che precede quella fornita quando viene chiamata la nuova funzione
 * */





/**
 * Call method
 * It can be used to invoke (call) a method with an owner object as an argument (parameter).
 * With call(), an object can use a method belonging to another object.
 * similar to apply()
 */
var person = {
    fullName: function () {
        return this.firstName + " " + this.lastName;
    }
}
var person1 = {
    firstName: "John",
    lastName: "Doe"
}
var person2 = {
    firstName: "Mary",
    lastName: "Doe"
}
person.fullName.call(person1);  // Will return "John Doe"




/**
 * Apply method
 * With the apply() method, you can write a method that can be used on different objects.
 * 
 * The call() method takes arguments separately.
 * The apply() method takes arguments as an array.
 */
var person = {
    fullName: function (city, country) {
        return this.firstName + " " + this.lastName + "," + city + "," + country;
    }
}
var person1 = {
    firstName: "John",
    lastName: "Doe"
}
person.fullName.apply(person1, ["Oslo", "Norway"]); // "John Doe,Oslo,Norway"
person.fullName.call(person1, "Oslo", "Norway"); // "John Doe,Oslo,Norway"




/**
 * Pure vs impure functions
 * 
 * Immutability comes when we want to preserve our state. 
 * To keep our state from changing we have to create a new instance of our state objects.
 */
var state = {
    prp: 'no'
};
function bad(state) {
    state.prp = 'yes'
    return state
}
function bad2(state) {
    return state.prp += '!'
}
function good(state) {
    // let newState = { ...state } ES6
    // newState.prp = 'yes'
    var newState = Object.assign({}, state, {prp: 'maybe'});
    return newState
}
function good2(state) {
    return Object.assign({}, state, {prp: state.prp + '!'});
}



/**
 *  IIFE - Immediately-invoked Function Expression
 * */
var f =(function () {
    'use strict';

    angular
        .module ('Module')
        .directive ('directive', directive);


    /** @ngInject */
    function directive() {

        function directiveController(){
            var vm = this;
            
            init();

            function init(){

            }
        }

        function link(){

        }

        return {
            bindToController: true,
            controller: directiveController,
            controllerAs: '$Ctrl',
            link: link,
            restrict: 'AE',
            scope: {},
        }
    }

} ());
