var MyClass = /** @class */ (function () {
    function MyClass(x, y) {
        this.x = x;
        this.y = y;
    }
    MyClass.prototype.sayHello = function () {
        console.log('Hello from', this.x, this.y);
    };
    MyClass.prototype.sayHelloAsync = function () {
        var _this = this;
        setTimeout(function () { return setTimeout(function () { return console.log('Hello from', _this.x, _this.y); }, 1000); });
    };
    return MyClass;
}());
var obj = new MyClass(1, 20);
obj.sayHello();
obj.sayHelloAsync();
