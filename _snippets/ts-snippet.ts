class MyClass {
    constructor(private x, private y) { }
    sayHello() {
        console.log('Hello from', this.x, this.y);
    }
    sayHelloAsync() {
        setTimeout(() => setTimeout(() => console.log('Hello from', this.x, this.y), 1000));
    }
}

const obj: MyClass = new MyClass(1, 20);
obj.sayHello();
obj.sayHelloAsync();



