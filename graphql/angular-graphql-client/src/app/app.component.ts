import { Apollo } from 'apollo-angular';
import { Component } from '@angular/core';
import gql from 'graphql-tag';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-root',
    template: '<pre>{{res | json}}</pre>'
})
export class AppComponent {
    res;
    constructor(apollo: Apollo) {
        const query = gql`
            query {
                blogposts {
                    title
                }
            }
        `;
        // const query = gql`
        //     query allFilms {
        //         allFilms {
        //             films {
        //                 id,
        //                 title,
        //                 starshipConnection {
        //                     totalCount,
        //                     starships {
        //                         name,
        //                         model
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // `;
        apollo.query({ query })
            .pipe(tap(console.log))
            .subscribe(res => this.res = res);
    }
}
