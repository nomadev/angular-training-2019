// https://malcoded.com/posts/graphql-with-angular-apollo/

const express = require('express')
const graphql = require('express-graphql')
const schema = require('./schema')
const cors = require('cors')
const bodyParser = require('body-parser')

const app = express()

app.use(
    '/graphql',
    cors(), 
    bodyParser.json(),
    graphql({
        schema: schema,
        graphiql: true,
    })
)

const server = app.listen(3000);
console.log(`listening at http://localhost:${server.address().port}/graphql`);