const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(bodyParser.json());


app.use(cors());

const successWheel = theshold => {
    theshold = theshold || 0.2;
    return Math.random() > theshold;
}

const SuccessAnswer = (message, data) => ({
    resultCode: 0,
    resultMessage: message || 'Success',
    data: data || undefined
})

const ErrorAnswer = (code, message) => ({
    resultCode: code || 9999,
    resultMessage: message || 'Generic error'
})

const badRequest = (res) => {
    return res.status(400).send(ErrorAnswer(400, 'Bad request'));
}

/**
 * newsletter subscription / unsubscription
 */
app.post("/newsletter/subscribe/:topic", (req, res, next) => {
    if(! req.params.topic || ! req.body.email) return badRequest(res);
    successWheel() ? res.json(SuccessAnswer()) : res.status(406).send(ErrorAnswer(406, 'Email already subscribed'))
});
app.delete("/newsletter/:email", (req, res, next) => {
    if(! req.params.email ) return badRequest(res);
    if(successWheel())
        res.json(SuccessAnswer(`${req.params.email} succesfully unsubscribed`))
    else
        res.status(500).send(ErrorAnswer())
});


/**
 * signup
 */
app.post("/signup", (req, res, next) => {
    if(! req.body.user) return badRequest(res);
    res.json(SuccessAnswer('user created', req.body.user))
})
app.post("/signup/checkEmail", (req, res, next) => {
    if(! req.body.email) return badRequest(res);
    successWheel(.4) ? res.json(SuccessAnswer()) : res.status(406).send(ErrorAnswer(406, 'Email already used'))
});

/**
 * login
 */
app.post("/user/login", (req, res, next) => {
    if(! req.body.email || ! req.body.password) return badRequest(res);
    res.json(SuccessAnswer(null, {id:2, email: req.body.email, permissions: ['user']}))
});
app.post("/user/logout", (req, res, next) => {
    res.json(SuccessAnswer());
});




// bootstrap
app.listen(3000, () => {
    console.log('Mock BE listening at http://localhost:3000');
});

