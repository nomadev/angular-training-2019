# Obiettivi
Si vuole creare un'applicazione per i fan di Star Wars.
L'app si interfaccerà con le API pubbliche SWAPI per reperire i dati

## Requisiti generali
Su tutte le pagine dell'app dovrebbero essere presenti le seguenti feature:
    - Header responsive
    - Footer responsive
    - Sidebar responsive
    - Possibilità di iscriversi alla newsletter
    - Gestione degli errori sui servizi tramite Snackbar https://material.angular.io/components/snack-bar/overview

## Requisiti homepage
- La homepage conterrà un elenco di risultati per le seguenti categorie:
    - Planets
    - Characters
    - Starships
    - Vehicles
- Dovrà essere possibile selezionare l'elenco che si vuole visualizzare; di default dovrebbe essere selezionata la prima categoria.
- Input di ricerca

## Plus
- Far corrispondere una route Angular ad una ricerca; es. localhost:4200/vehicles. In questo modo, in base all'url, l'app saprà quale ricerca mostrare, piuttosto che il risultato della prima categoria di default.
- Gestire la paginazione dei risultati di ricerca
- Sviluppare una pagina statistics dove poter visualizzare un grafico (utilizzando DxChartModule https://js.devexpress.com/Demos/WidgetsGallery/Demo/Charts/SideBySideBar/Angular/MaterialPurpleDark/)
    - Il grafico dovrebbe mettere a confronto i vari tipi di dato sulle rispettive proprietà numeriche; es Planet: rotation_period, orbital_period, population
- Registrazione / login per poter accedere alla pagina statistics
- Un tema css chiaro forse non è la scelta adatta per una fan page su Star Wars. Creare un custom Material Theme usando la funzione mat-dark-theme. https://material.angular.io/guide/theming

- Creare un form di registrazione per l'utente premium. La registrazione richiede i seguenti campi
    - Nome
    - Cognome
    - Email (controllo async per prevenire registrazioni multiple)
    - Età (controllo maggiori di 12 anni)
    - google captcha anti spam
    - checkbox
        - obbligatoria termini e condizioni
        - obbligatoria privacy policy
        - facoltativa marketing (pre-selezionata)