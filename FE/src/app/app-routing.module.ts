import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./modules/homepage/homepage.module').then(m => m.HomepageModule)
    },
    {
        path: 'details',
        loadChildren: () => import('./modules/element-details/element-details.module').then(m => m.ElementDetailsModule)
    },
    {
        path: 'statistics',
        loadChildren: () => import('./modules/statistics/statistics.module').then(m => m.StatisticsModule)
    },
    {
        path: 'signup',
        loadChildren: () => import('./modules/signup/signup.module').then(m => m.SignupModule)
    },
    // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    /**
     * If the RouterModule didn’t have forRoot() then each feature module would instantiate a new Router instance,
     * which would break the application as there can only be one Router.
     * By using the forRoot() method, the root application module imports RouterModule.forRoot(...)
     * and gets a Router, and all feature modules import RouterModule.forChild(...) which does not instantiate another Router.
     */
    imports: [RouterModule.forRoot(routes)],
})
export class AppRoutingModule { }
