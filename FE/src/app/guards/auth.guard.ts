import { Injectable } from '@angular/core';
import { includes } from 'lodash';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    Router,
    CanActivateChild,
    CanLoad,
    UrlSegment
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { UserService } from '../services/user.services';
import { PermissionsService } from '../modules/permissions/permissions.service';
import { Route } from '@angular/compiler/src/core';



@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(
        private permissionService: PermissionsService,
        private router: Router,
    ) { }


    private isAuthorized(route: ActivatedRouteSnapshot): Observable<boolean> {
        return of(true);
        // if (!route.data) { return of(true); }
        // const { permissions, redirectTo } = route.data;
        // return this.permissionService.hasPermission(permissions).pipe(
        //     tap(isAuth => {
        //         if (!isAuth) {
        //             this.router.navigateByUrl(redirectTo);
        //         }
        //     })
        // );
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.isAuthorized(route);
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> {
        return this.isAuthorized(route);
    }
}
