
export enum SearchType {
    Planets = 'planets',
    People = 'people',
    Starship = 'starship',
    Vehicle = 'vechicle'
}

export class SearchForm {
    constructor(
        public type: SearchType,
        public query?: string
    ) {}
}