export interface Planet {
    climate: string;
    created: string;
    diameter: number;
    edited: string;
    films: string[];
    gravity: number;
    name: string;
    orbital_period: number;
    population: number;
    residents: string[];
    rotation_period: number;
    surface_water: number;
    terrain: string;
    url: string[];
}
