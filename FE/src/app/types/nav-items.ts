export interface NavItem {
    url: string;
    label: string;
    service: string;
    elementPreview: string[];
};
