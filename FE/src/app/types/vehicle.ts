export interface Vehicle {
    cargo_capacity: 50000;
    consumables: string;
    cost_in_credits: 150000;
    created: string;
    crew: 46;
    edited: string;
    length: 36.8;
    manufacturer: string;
    max_atmosphering_speed: 30;
    model: string;
    name: string;
    passengers: 30;
    pilots: string[];
    films: string[];
    url: string[];
    vehicle_class: string;
}