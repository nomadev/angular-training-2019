import { Permission } from './../modules/permissions/permissions';
export class User {
    constructor(
        public permissions?: Permission[],
        public id?: string,
        public name?: string,
        public surname?: string,
        public birday?: Date,
        public token?: string,
        public email?: string,
    ) {}
}
