import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DuckgogoService {
    constructor(private http: HttpClient) { }

    private url = 'https://api.duckduckgo.com';

    private paramsFactory = (query: string): HttpParams => {
        return new HttpParams()
            .set('format', 'json')
            .set('no_redirect', '1')
            .set('pretty', '1')
            .set('q', query);
    }

    get(query: string) {
        return this.http.get(this.url, {params: this.paramsFactory(query)});
    }
}
