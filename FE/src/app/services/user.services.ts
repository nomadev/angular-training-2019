import { UtilsService } from './utils.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../types/user';
import { filter, map, tap, take } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private urlFactory = UtilsService.urlFactory('http://localhost:3000');

    private user = new BehaviorSubject<User>(null);
    public readonly user$ = this.user.asObservable().pipe(filter(u => Boolean(u)));
    public readonly userHeaders$ = this.user$.pipe(
        map(u => ({
            token: u.token,
            userId: u.id,
        }))
    );

    private guestUser = new User([{ code: 'guest', read: true, write: true }]);

    constructor(
        private http: HttpClient
    ) {
        const savedUser: User = JSON.parse(localStorage.getItem('user'));
        if (savedUser) {
            this.user.next(savedUser);
        } else {
            const u: User = this.guestUser;
            this.user.next(u);
        }
    }

    login(email: string, password: string): Observable<User> {
        return this.http.post(this.urlFactory('/user/login'), { email, password })
            .pipe(
                map((resp: any) => resp.data),
                tap(u => this.user.next(u)),
                tap(u => localStorage.setItem('user', JSON.stringify(u))),
                take(1)
            );
    }


    logout() {
        localStorage.removeItem('user');
        this.user.next(this.guestUser);
    }

}
