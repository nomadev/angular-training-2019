import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpResponse, HttpEvent } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RequestCacheService {

    /**
     * Map to store the cached HttpRequests
     */
    private cache = new Map<string, [Date, HttpResponse<any>]>();

    /**
     * the cacheQueue must be public to be filled from outside
     */
    cacheQueue = new Map<string, Observable<HttpEvent<any>>>();

    get(key): Observable<HttpEvent<any>> {
        const tuple = this.cache.get(key);
        if (!tuple) {
            if (this.cacheQueue.has(key))
                /**
                 * The HttpRequest has been made but not yet resolved.
                 * We return the cached Observable
                 */
                return this.cacheQueue.get(key);
            else {
                /**
                 * In this case we need a
                 * new HttpRequest
                 */
                return null;
            }
        }

        const expires = tuple[0];
        const httpResponse = tuple[1];
        const now = new Date();

        if (expires && expires.getTime() < now.getTime()) {
            /**
             * The cache has expired
             * a new HttpRequest will be performed
             */
            this.cache.delete(key);
            return null;
        }

        /**
         * The cached result is returned
         * as new Observable
         */
        return of(httpResponse);
    }

    set(key, value, ttl = null) {
        if (ttl) {
            const expires = new Date();
            expires.setSeconds(expires.getSeconds() + ttl);
            this.cache.set(key, [expires, value]);
        } else {
            this.cache.set(key, [null, value]);
        }
        this.cacheQueue.delete(key);
    }

    delete(key) {
        this.cacheQueue.delete(key);
        this.cache.delete(key);
    }

    clear() {
        this.cacheQueue.clear();
        this.cache.clear();
    }
}
