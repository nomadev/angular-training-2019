import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilsService } from './utils.service';

@Injectable({
    providedIn: 'root'
})
export class NodeBeService {
    constructor(private http: HttpClient) { }

    private urlFactory = UtilsService.urlFactory('http://localhost:3000');

}
