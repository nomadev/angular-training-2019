import { Injectable } from '@angular/core';
import { of, timer, Observable, Subject, AsyncSubject, BehaviorSubject } from 'rxjs';
import { tap, switchMap, map, take, filter } from 'rxjs/operators';

export interface AlertUser {
    id?: number;
    type: string;
    message: string;
    manualDismiss?: boolean;
    dismissTime?: number; // in milliseconds
    ts?: Date;
    _?: any;
}

export interface UserResponse {
    result: boolean;
    response: any;
    _ts?: Date;
}


@Injectable({
    providedIn: 'root'
})
export class AlertUserService {

    private sending = false;
    /**
     * la coda di alert è composta da un AsyncSubject e da un BehaviourSubject.
     * Ciò che viene ritornato dalla funzione pubblica "send" è l'AsyncSubject. Tale scelta
     * è dovuta alla proprietà di AsyncSubject di emettere il suo valore agli observer solo DOPO che il subject viene
     * completato
     * Usiamo il BehaviourSubject perchè ci permette di avere l'inizializzazione direttamente con un contenuto (l'alert, in questo caso) dentro il metodo send
     * Usiamo un semplice subject per contenere il messaggio, perchè non ha la necessità di essere inizializzato subito con un contenuto
     */
    private queue: [AsyncSubject<UserResponse>, BehaviorSubject<AlertUser>, string] [] = [];
    /**
     * conserva l'alert da mostrare.
     */
    private subject: Subject<AlertUser> = new Subject();
    /**
     * Il subject contenente il messaggio viene esposto pubblicamente come un observable readonly
     */
    public readonly message = this.subject.asObservable();


    private sender(): Observable<boolean> {
        // prendo il primo elemento della coda
        const a$ = this.queue.shift();
        if (! a$ ) this.sending = false;
        else {
            /** se la coda non è vuota compongo un observable che inizia con un timer di 500ms.
             * Il timer serve anche come piccola pausa tra questo alert e l'eventuale precedente
             */
            this.sending = true;
            timer(500).pipe(
                 /**
                 * senza un take(1), siccome si tratta di un hot Observable, avremmo un leak
                 */
                take(1),
                /**
                 * passo dall'observable timer al BS del messaggio da mostrare
                 */
                switchMap(res => a$[1]),
                /**
                 * assegno al subject esposto pubblicamente, il contenuto di questo alert
                 */
                tap( res => this.subject.next(res)),
                /**
                 * Mentre il messaggio è mostrato, faccio partire un timer di 5 secondi
                 */
                switchMap(res => timer(2500)),
                /**
                 * Passati i 5 secondi, rimuovo il messaggio dal subject esposto
                 */
                tap( res => this.subject.next(null)),
                /**
                 * Completo l'asyncSubject così l'observer ha la notifica che l'alert è stato mostrato
                 */
                tap( res => a$[0].complete()),
                tap( res => this.sending = false),
                 /**
                 * se la coda non è vuota, richiamo la stessa funzione sender
                 */
                filter( res => this.queue.length > 0 ),
                tap( res => this.sender())
            )
            .subscribe();
        }
        return of(this.sending);
    }


    send(alert: AlertUser): AsyncSubject<UserResponse> {
        /**
         * Quando viene inviato un nuovo alert, viene creato un record all'interno della coda.
         * La risposta da inviare agli observer è salvata nell'AysncSubject che viene tornato.
         * L'alert vero e proprio viene messo in coda all'interno di un BehaviourSubject
         */
        if (this.queue.find( o => o[2] === alert.message)) return null;

        const a$: [AsyncSubject<UserResponse>, BehaviorSubject<AlertUser>, string] =
                  [new AsyncSubject<UserResponse>(), new BehaviorSubject<AlertUser>(alert), alert.message];

        a$[0].next({result: true, response: alert});

        this.queue.push(a$);

        if (! this.sending)
            this.sender();

        return a$[0];
    }
}
