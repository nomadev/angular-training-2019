import { UtilsService } from './utils.service';
import { Vehicle } from './../types/vehicle';
import { Starship } from './../types/starship';
import { Person } from '../types/person';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { Planet } from '../types/planet';

@Injectable({
    providedIn: 'root'
})
export class SwapiService {

    constructor(private http: HttpClient) { }

    private url = 'https://swapi.co/api/';

    private urlFactory = UtilsService.urlFactory(this.url);

    private errorHandler(err) {
        console.error(err);
        return of(err);
    }

    buildUrl<T>(...args): Observable<any> {
        return this.http.get<T>(this.urlFactory(...args))
            .pipe(
                catchError(this.errorHandler),
                tap(res => console.log(res))
            );
    }

    getUrl<T>(url: string, params?: HttpParams): Observable<any> {
        return this.http.get<T>(url, {params})
            .pipe(
                catchError(this.errorHandler),
                tap(res => console.log(res))
            );
    }

    getPeople(id?: number): Observable<Person | Person[]> {
        return this.buildUrl<Person | Person[]>('people', id);
    }
    getStarship(id?: number): Observable<Starship | Starship[]> {
        return this.buildUrl<Starship | Starship[]>('starships', id);
    }
    getVehicle(id?: number): Observable<Vehicle | Vehicle[]> {
        return this.buildUrl<Vehicle | Vehicle[]>('vehicles', id);
    }
    getPlanets(id?: number): Observable<Planet | Planet[]> {
        return this.buildUrl<Planet | Planet[]>('planets', id);
    }

    search(what: string, query: string): Observable<any> {
        const params = new HttpParams().set('search', query);
        return this.getUrl(this.urlFactory(what), params);
    }
}

