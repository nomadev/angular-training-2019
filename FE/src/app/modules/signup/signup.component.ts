import { EMAIL_REGEX } from './../../utils/regex';
import { DynamicFormService } from './../forms/dynamic-form/dynamic-form.service';
import { CustomInput, CustomSelect, CustomTextArea, CustomRadio, CustomCheckbox } from './../forms/form-field';
import { Validators, FormGroup } from '@angular/forms';
import { AppSidebarOptions } from './../shared/components/sidebar/sidebar.component';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

    constructor(private fs: DynamicFormService) { }

    @ViewChild('form', { static: true }) formRef;

    sidenavOptions: AppSidebarOptions = {
        showToggle: false,
        opened: false
    };

    formModel = {
        title: new CustomSelect({ id: 'title', label: 'Title', validators: [Validators.required], options: [{ key: 'mr', value: 'Mr' }, { key: 'ms', value: 'Ms' }] }),
        name: new CustomInput({ id: 'name', label: 'Name', type: 'text', validators: [Validators.required] }),
        surname: new CustomInput({ id: 'surname', label: 'Surname', type: 'text', validators: [Validators.required] }),
        email: new CustomInput({ id: 'email', label: 'Email', type: 'email', validators: [Validators.required, Validators.pattern(EMAIL_REGEX)] }),
        notes: new CustomTextArea({ id: 'notes', label: 'Notes' }),
        gender: new CustomRadio({
            id: 'gender',
            label: 'Gender',
            values: [
                { key: 'male', value: 'Male' },
                { key: 'female', value: 'Female' }
            ]
        }),
        privacy: new CustomCheckbox({ id: 'privacy', label: 'Privacy policy', validators: [Validators.required, Validators.pattern('true')] }),
        tos: new CustomCheckbox({ id: 'tos', label: 'Terms of services', validators: [Validators.required, Validators.pattern('true')] }),
        marketing: new CustomCheckbox({ id: 'marketing', label: 'Marketing', value: true })
    };
    form: FormGroup = this.fs.executeModel(this.formModel);


    ngOnInit() {
    }

    registerUser(form) {
        console.log(form.value);
    }

}
