import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ElementDetailsComponent } from './element-details.component';

const routes: Routes = [
    {path: '',  redirectTo: 'vehicles'},
    {path: ':type/:name', component: ElementDetailsComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)]
})
export class ElementDetailsRoutingModule { }
