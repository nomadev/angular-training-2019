import { SharedModule } from './../shared/shared.module';
import { ElementDetailsRoutingModule } from './element-details-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElementDetailsComponent } from './element-details.component';

@NgModule({
  declarations: [ElementDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    ElementDetailsRoutingModule
  ]
})
export class ElementDetailsModule { }
