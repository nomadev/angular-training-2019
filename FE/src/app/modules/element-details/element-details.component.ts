import { ActivatedRoute } from '@angular/router';
import { AppSidebarOptions } from './../shared/components/sidebar/sidebar.component';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-element-details',
    template: `
    <app-sidebar
        [options]="sidenavOptions"
    >
        <button mat-icon-button routerLink="/" class="header-content">Home</button>
        <p class="blockCenter page-content">{{data.name}}</p>
    </app-sidebar>
  `,
    styleUrls: ['./element-details.component.scss']
})
export class ElementDetailsComponent implements OnInit {

    sidenavOptions: AppSidebarOptions = {
        showToggle: false,
        opened: false
    };
    data: any = {};

    constructor(private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.params.subscribe(res => this.data = res);
    }

}
