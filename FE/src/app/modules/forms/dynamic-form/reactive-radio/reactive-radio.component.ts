import { ControlContainer, FormGroupDirective } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ReactiveField } from '../reactive-field';

@Component({
  selector: 'app-reactive-radio',
  templateUrl: './reactive-radio.component.html',
  styleUrls: ['./reactive-radio.component.scss'],
  viewProviders: [
      {
          provide: ControlContainer,
          useExisting: FormGroupDirective
      }
  ]
})
export class ReactiveRadioComponent extends ReactiveField {}
