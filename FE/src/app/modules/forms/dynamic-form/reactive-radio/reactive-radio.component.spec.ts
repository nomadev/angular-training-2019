import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveRadioComponent } from './reactive-radio.component';

describe('ReactiveRadioComponent', () => {
  let component: ReactiveRadioComponent;
  let fixture: ComponentFixture<ReactiveRadioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactiveRadioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
