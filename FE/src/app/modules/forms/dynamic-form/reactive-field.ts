import { FormGroup, AbstractControl } from '@angular/forms';
import { Input, OnChanges } from '@angular/core';
import { CustomFormField } from '../form-field';

export abstract class ReactiveField implements OnChanges {
    @Input() parentForm: FormGroup;
    @Input('field') field: CustomFormField<any>;
    formControl: AbstractControl;

    ngOnChanges(): void {
        try {
            this.formControl = this.parentForm.controls[this.field.id];
        } catch (error) {
            console.error('no form control defined', error);
        }
    }

}