import { FormBuilder, FormGroup } from '@angular/forms';
import { Injectable } from '@angular/core';
import { CustomFormField } from '../form-field';

@Injectable({
    providedIn: 'root'
})
export class DynamicFormService {
    constructor(
        private fb: FormBuilder
    ) { }
    executeModel(formModel: CustomFormField<any>[] | { [key: string]: CustomFormField<any> }): FormGroup {

        const formArray = formModel[0] ? formModel as CustomFormField<any>[] : Object.keys(formModel).map(k => formModel[k]);
        /**
         * build dynamically the FormGroup
         */
        const f = {};
        formArray.forEach(field => {
            f[field.id] = [field.value, field.validators, field.asyncValidators];
        });
        return this.fb.group(f);
    }
}
