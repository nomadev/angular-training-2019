import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { CustomFormField } from '../../form-field';
import { ControlContainer, FormGroupDirective, FormGroup, AbstractControl } from '@angular/forms';
import { ReactiveField } from '../reactive-field';

@Component({
    selector: 'app-reactive-input',
    templateUrl: './reactive-input.component.html',
    styleUrls: ['./reactive-input.component.scss'],
    viewProviders: [
        {
            provide: ControlContainer,
            useExisting: FormGroupDirective
        }
    ]
})
export class ReactiveInputComponent extends ReactiveField {
}
