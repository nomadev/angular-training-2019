import { DynamicFormService } from './dynamic-form.service';
import { CustomFormField } from '../form-field';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Input, Output, EventEmitter, AfterContentInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';


/**
 * Example of formModel input:
 * [
 *      new CustomInput({ id: 'input1', label: 'id', type: 'number', placeholder: 'id!', validators: [Validators.required] }),
 *      new CustomInput({ id: 'input2', label: 'testo', validators: [Validators.required], asyncValidator: [myAsyncValidator] }),
 *      new CustomSelect({ id: 'selectSomething', label: 'seleziona', options: [
 *          {value: 1, label: 'something'},
 *          {value: 2, label: 'somethingElse'},] }),
 *      new CustomTextArea({ id: 'longDescription', label: 'Write us!', rows: 2 }),
 * ]
 *
 */

export class DynamicFormConfig {
    public showSubmit: boolean;
    public showCancel: boolean;
    constructor(
        options: {
            showSubmit?: boolean,
            showCancel?: boolean,
        } = { showSubmit: true, showCancel: false }
    ) {
        this.showSubmit = options.showSubmit;
        this.showCancel = options.showCancel;
    }
}

@Component({
    selector: 'app-dynamic-form',
    templateUrl: './dynamic-form.component.html',
    styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit, AfterContentInit {

    formReady: boolean;
    form: FormGroup = new FormGroup({});

    @Input() formModel: CustomFormField<any>[] = [];
    @Input() config: DynamicFormConfig = new DynamicFormConfig();
    @Output() formSubmission: EventEmitter<any> = new EventEmitter();

    constructor(
        private fs: DynamicFormService
    ) {
        this.formReady = false;
    }

    ngOnInit() {
    }

    ngAfterContentInit() {
        if (this.formModel.length > 0) {
            this.form = this.fs.executeModel(this.formModel);
        }
        this.formReady = true;
    }

    onSubmit() {
        if (!this.form.valid) { return; }
        this.formSubmission.emit(this.form.value);
    }

}
