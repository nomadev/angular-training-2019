import { ControlContainer, FormGroupDirective } from '@angular/forms';
import { Component } from '@angular/core';
import { ReactiveField } from '../reactive-field';

@Component({
  selector: 'app-reactive-textarea',
  templateUrl: './reactive-textarea.component.html',
  styleUrls: ['./reactive-textarea.component.scss'],
  viewProviders: [
      {
          provide: ControlContainer,
          useExisting: FormGroupDirective
      }
  ]
})
export class ReactiveTextareaComponent extends ReactiveField {

}
