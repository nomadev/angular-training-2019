import { ControlContainer, FormGroupDirective } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ReactiveField } from '../reactive-field';

@Component({
  selector: 'app-reactive-checkbox',
  templateUrl: './reactive-checkbox.component.html',
  styleUrls: ['./reactive-checkbox.component.scss'],
  viewProviders: [
      {
          provide: ControlContainer,
          useExisting: FormGroupDirective
      }
  ]
})
export class ReactiveCheckboxComponent extends ReactiveField {}
