import { ControlContainer, FormGroupDirective } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ReactiveField } from '../reactive-field';

@Component({
  selector: 'app-reactive-select',
  templateUrl: './reactive-select.component.html',
  styleUrls: ['./reactive-select.component.scss'],
    viewProviders: [
        {
            provide: ControlContainer,
            useExisting: FormGroupDirective
        }
    ]
})
export class ReactiveSelectComponent extends ReactiveField {

}
