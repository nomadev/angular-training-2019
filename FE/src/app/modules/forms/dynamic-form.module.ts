import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { DisableControlDirective } from './disable-control.directive';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { ReactiveInputComponent } from './dynamic-form/reactive-input/reactive-input.component';
import { ReactiveSelectComponent } from './dynamic-form/reactive-select/reactive-select.component';
import { FieldErrorsComponent } from './field-errors/field-errors.component';
import { ReactiveTextareaComponent } from './dynamic-form/reactive-textarea/reactive-textarea.component';
import { ReactiveRadioComponent } from './dynamic-form/reactive-radio/reactive-radio.component';
import { ReactiveCheckboxComponent } from './dynamic-form/reactive-checkbox/reactive-checkbox.component';

const modules = [
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCheckboxModule
];
const declarations = [
    FileUploadComponent,
    DynamicFormComponent,
    DisableControlDirective,
    ReactiveInputComponent,
    ReactiveSelectComponent,
    FieldErrorsComponent,
    ReactiveTextareaComponent,
    ReactiveRadioComponent,
    ReactiveCheckboxComponent
];
const entryComponents = [];


@NgModule({
    declarations,
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ...modules
    ],
    exports: [
        ...modules,
        ...declarations,
        ...entryComponents
    ]
})
export class DynamicFormModule { }
