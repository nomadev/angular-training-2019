import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilsService } from './../../services/utils.service';
import { AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AsyncValidationService {

    constructor(
        private http: HttpClient
    ) {}

    private urlFactory = UtilsService.urlFactory('http://localhost:3000');

    email(): AsyncValidatorFn {
        return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {
            return this.http.post(this.urlFactory('signup', 'checkEmail'), {email: control.value});
        };
    }
}
