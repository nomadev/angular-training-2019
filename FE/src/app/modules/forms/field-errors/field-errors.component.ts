import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-field-errors',
    templateUrl: './field-errors.component.html',
    styleUrls: ['./field-errors.component.scss'],
})
export class FieldErrorsComponent implements OnInit {

    @Input('fieldControl') fieldControl: FormControl;

    constructor() { }

    ngOnInit() {
    }

    objKeys(obj) {
        return Object.keys(obj);
    }

}
