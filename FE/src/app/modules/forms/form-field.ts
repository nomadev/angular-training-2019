import { concatMap } from 'rxjs/operators';
import { Validator, AsyncValidatorFn, Validators } from '@angular/forms';

export abstract class CustomFormField<T> {
    id: string;
    controlType?: string;
    value?: T;
    label?: string;
    disabled?: boolean;
    validators?: Validators[];
    asyncValidators?: AsyncValidatorFn[];
    placeholder?: string;

    constructor(options: {
        id: string,
        controlType?: string,
        value?: T,
        label?: string,
        placeholder?: string,
        disabled?: boolean,
        validators?: Validators[],
        asyncValidators?: AsyncValidatorFn[]
    } = {id: null}) {
        this.id = options.id;
        this.controlType = options.controlType;
        this.value = options.value;
        this.label = options.label;
        this.placeholder = options.placeholder || '';
        this.disabled = options.disabled || false;
        this.validators = options.validators || [];
        this.asyncValidators = options.asyncValidators || [];
    }
}

export class CustomInput extends CustomFormField<string> {
    type: string;
    constructor(options) {
        super(options);
        this.controlType = 'inputText';
        this.type = options.type || 'text';
    }
}


export class CustomInputFile extends CustomFormField<File> {
    type: string;
    accept: string;
    constructor(options) {
        super(options);
        this.value = options.value || null;
        this.type = 'file';
        this.controlType = 'inputFile';
        this.accept = options.accept || '*';
    }
}

export class CustomTextArea extends CustomFormField<string> {
    rows: number;
    resize: boolean;
    constructor(options) {
        super(options);
        this.controlType = 'textarea';
        this.rows = options.rows || 0;
        this.resize = options.resize || false;
    }
}

export class CustomSelect extends CustomFormField<string> {
    options: { key: string, value: string }[] = [];
    constructor(options) {
        super(options);
        this.controlType = 'select';
        this.options = options.options || [];
    }
}

export class CustomRadio extends CustomFormField<string> {
    values: { key: string, value: string }[] = [];
    constructor(options) {
        super(options);
        this.controlType = 'radio';
        this.values = options.values || [];
    }
}

export class CustomCheckbox extends CustomFormField<string> {
    constructor(options) {
        super(options);
        this.controlType = 'checkbox';
    }
}
