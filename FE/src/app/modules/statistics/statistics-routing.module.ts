import { AuthGuard } from './../../guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatisticsComponent } from './statistics.component';

const routes: Routes = [
    {
        canActivate: [AuthGuard],
        data: {
            permissions: ['user'],
        },
        path: '',
        component: StatisticsComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)]
})
export class StatisticsRoutingModule { }
