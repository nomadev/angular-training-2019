import { PermissionsService } from './permissions.service';


import { Directive, Input, ViewContainerRef, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { Permission } from './permissions';
/**
 * Structural directive based on user returned array of Permission
 *
 * Standard usage is "user has at least one of the passed permissions":
 *      <div *appHasPermission="['CODE1', 'CODE2', {code: 'CODE3', read: true, write: true}]"></div>
 *
 * This directive can also be used to check if user has ALL permissions recieved
 *      <div *appHasPermission="null;hasAll:['CODE1', {code: 'CODE2', read: true, write: true}]"></div>
 *
 * When only <Permission> f.code is passed, the check is made just on the read permission
 * To check also for the write permission, pass a full Permission object
 *
 */


@Directive({
    selector: '[appHasPermission]'
})
export class HasPermissionDirective implements OnInit, OnDestroy {
    // tslint:disable-next-line:no-input-rename
    @Input('appHasPermission') hasAtLeastPermission: (string | Permission)[];
    // tslint:disable-next-line:no-input-rename
    @Input('appHasPermissionHasAll') hasAllPermissions: (string | Permission)[];
    private subscription: Subscription;

    constructor(
        private vcr: ViewContainerRef,
        private tpl: TemplateRef<any>,
        private permissionsService: PermissionsService
    ) { }

    ngOnInit() {
        const permissionQuery$ = this.hasAtLeastPermission ?
            this.permissionsService.hasPermission(this.hasAtLeastPermission) :
            this.permissionsService.hasAllPermission(this.hasAllPermissions);
        this.subscription = permissionQuery$
            .subscribe(hasPermission => {
                this.vcr.clear();
                if (hasPermission) {
                    this.vcr.createEmbeddedView(this.tpl);
                }
            });
    }

    ngOnDestroy() {
        if (this.subscription) this.subscription.unsubscribe();
    }
}
