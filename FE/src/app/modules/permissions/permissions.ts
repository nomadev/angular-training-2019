export class Permission {
        constructor(
            public code?: string,
            public descrizione?: string,
            public read?: boolean,
            public write?: boolean) {}
}
