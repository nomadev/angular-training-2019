import { HasPermissionDirective } from './has-permission.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const imports = [
    CommonModule
];
const declarations = [
    HasPermissionDirective
];

@NgModule({
  declarations,
  imports,
  exports: [...declarations, ...imports]
})
export class PermissionsModule { }
