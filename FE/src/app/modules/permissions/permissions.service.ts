import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Permission } from './permissions';
import { UserService } from 'src/app/services/user.services';


@Injectable({ providedIn: 'root' })
export class PermissionsService {
    public readonly userPermissions$ = this.userService.user$.pipe(
        map(u => u.permissions)
    );

    constructor(private userService: UserService) { }


    /**
     * Is implicit that if permissionCodes is an array of string, the returning array of Permission
     * will have only read set to true
     * To check the write permission, pass a full Permission object with write set to true
     */
    private permissionFactory(permissionCodes: (string | Permission)[]): Permission[] {
        try {
            return permissionCodes.map(el => typeof el === 'string' ? new Permission(el, null, true, false) : el);
        } catch (error) {
            console.error('invalid permission codes provided', permissionCodes);
        }
    }

    private standardCheck(permission: Permission, userPermissions: Permission[]): boolean {
        return userPermissions.findIndex(profile => {
            // check if there is a rule in profile permissions
            const check = permission.code === profile.code;
            if (!check) { return check; }
            // at least read
            if (!profile.read) { return false; }
            // check write when required
            if (permission.write && !profile.write) { return false; }
            return check;
        }) > -1;
    }

    /**
     * checks if the user has all the permission required and return a boolean Observable
     */
    hasAllPermission(permissionCodes: (string | Permission)[]): Observable<boolean> {
        return this.userPermissions$.pipe(
            // tslint:disable-next-line:max-line-length
            map(userPermissions => this.permissionFactory(permissionCodes).every(permission => this.standardCheck(permission, userPermissions)) )
        );
    }

    /**
     * checks if the user has at least one of the permission required and return a boolean Observable
     */
    hasPermission(permissionCodes: (string | Permission)[]): Observable<boolean> {
        return this.userPermissions$.pipe(
            // tslint:disable-next-line:max-line-length
            map(userPermissions => this.permissionFactory(permissionCodes).some(permission => this.standardCheck(permission, userPermissions)) )
        );
    }
}
