import { HomeSubpagesMap } from './homepage.config';
import { HomeSubpages } from './homepage.config';
import { NodeBeService } from '../../services/node-be.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SwapiService } from 'src/app/services/swapi.service';
import { flatMap, switchMap, map, concatMap, tap, take } from 'rxjs/operators';
import { Person } from 'src/app/types/person';
import { SearchForm } from 'src/app/types/seatch';
import { DuckgogoService } from 'src/app/services/duckgogo.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { NavItem } from 'src/app/types/nav-items';



@Component({
    selector: 'app-homepage',
    template: `
        <app-sidebar
            [navItem]="subpages"
        >
            <p class="blockCenter header-content">welcome home spece traveler</p>

            <div class="page-content">
                <div class="justify-contents">
                    <app-model-driven-search
                        (validSearch)="makeSearch($event)"
                    ></app-model-driven-search>
                </div>

                <div class="container" *ngIf="! loading; else loadingTpl">
                    <app-element-preview
                        [context]="subpage"
                        [dataSource]="response.results"
                        [displayedColumns]="elementPreviewColumns"
                    ></app-element-preview>
                </div>

                <ng-template #loadingTpl>
                    <app-loading-spinner></app-loading-spinner>
                </ng-template>

                <div *appHasPermission="['guest2']">solo per i guest!</div>

                <app-footer *ngIf="! loading"></app-footer>
            </div>
        </app-sidebar>
    `,
    styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit, OnDestroy {

    constructor(
        private activatedRoute: ActivatedRoute,
        private swapi: SwapiService,
        private duckgogo: DuckgogoService,
        private app: NodeBeService) {
        // this.swapi.getPeople()
        // .pipe(
        //     flatMap(  person  => {
        //         const p = person as Person;
        //         return this.swapi.getUrl((p.homeworld));
        //     },
        //     (person, planet) => ({person, planet}) )
        // )
        // .subscribe(res => {
        //     console.log(res);
        //     this.data = res;
        // });
    }
    elementPreviewColumns: string[];
    subs: Subscription[] = [];
    subpage: string;
    loading: boolean;
    response: any;
    subpages: NavItem[] = HomeSubpages;

    makeSearch(form: SearchForm) {
        const { query } = form;
        this.swapi.search(HomeSubpagesMap.get(this.subpage).service, query)
            .pipe(take(1))
            // .pipe(
            //     flatMap(results => this.duckgogo.get(query),
            //     (swapiRes, duckgogoRes) => ({
            //         swapi: swapiRes,
            //         duckgogo: duckgogoRes
            //     }))
            // )
            .subscribe(res => this.response = res);

    }

    ngOnInit(): void {
        this.subs.push(
            this.activatedRoute.params
                .pipe(
                    tap(p => this.loading = true),
                    map( params => this.subpage = params.subpage),
                    tap( subpage => this.elementPreviewColumns = HomeSubpagesMap.get(subpage).elementPreview),
                    concatMap( (subpage) => this.swapi.buildUrl(HomeSubpagesMap.get(subpage).service) )
                )
                .subscribe(res => {
                    this.response = res;
                    this.loading = false;
                })
        );
    }

    ngOnDestroy(): void {
        this.subs.map(s => s.unsubscribe());
    }
}
