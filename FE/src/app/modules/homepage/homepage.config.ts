import { NavItem } from 'src/app/types/nav-items';


export const HomeSubpages: NavItem[] = [
    {
        url: '/home/characters',
        label: 'Characters',
        service: 'people',
        elementPreview: ['name', 'gender', 'eye_color', 'hair_color', 'height']
    },
    {
        url: '/home/planets',
        label: 'Planets',
        service: 'planets',
        elementPreview: ['name', 'climate', 'created', 'diameter'],
    },
    {
        url: '/home/starships',
        label: 'Starships',
        service: 'starships',
        elementPreview: ['name', 'model', 'cargo_capacity', 'passengers']
    },
    {
        url: '/home/vehicles',
        label: 'Vehicles',
        service: 'vehicles',
        elementPreview: ['name', 'model', 'cargo_capacity', 'passengers']
    },
];

/**
 * data una sotto pagina, ritorna una NavItem
 */
export const HomeSubpagesMap: Map<string, NavItem> = (() => {
    const pageMap = HomeSubpages.map(p => [p.label.toLowerCase(), p]) as [string, NavItem][];
    return new Map(pageMap);
})();
