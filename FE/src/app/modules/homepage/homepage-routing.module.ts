import { HomepageComponent } from './homepage.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'home/characters'},
    {path: 'home', redirectTo: 'home/characters'},
    // {path: 'home', redirectTo: 'home/planets'},
    {path: 'home', children: [
        {
            path: ':subpage',
            component: HomepageComponent,
        },
        {
            path: ':subpage',
            component: HomepageComponent,
        }
    ]}
];

@NgModule({
    /**
     * The CLI also adds RouterModule.forChild(routes) to feature routing modules.
     * This way, Angular knows that the route list is only responsible for providing additional routes
     * and is intended for feature modules. You can use forChild() in multiple modules.
     */
    imports: [RouterModule.forChild(routes)]
})
export class HomepageRoutingModule { }
