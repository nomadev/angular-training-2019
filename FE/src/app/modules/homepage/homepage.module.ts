import { PermissionsModule } from './../permissions/permissions.module';
import { SharedModule } from './../shared/shared.module';
import { HomepageComponent } from './homepage.component';
import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import { HomepageRoutingModule } from './homepage-routing.module';




@NgModule({
    declarations: [
        HomepageComponent,
    ],
    imports: [
        CommonModule,
        HomepageRoutingModule,
        SharedModule,
        PermissionsModule
    ],
    providers: [],
})
export class HomepageModule { }
