import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
    selector: 'app-element-preview',
    templateUrl: './element-preview.component.html',
    styleUrls: ['./element-preview.component.sass']
})
export class ElementPreviewComponent implements OnInit {
    @Input() displayedColumns: string[] = ['model', 'name', 'cargo_capacity', 'passengers'];
    @Input() dataSource = [];
    @Input() context: string;

    constructor() { }

    ngOnInit() {
    }


}
