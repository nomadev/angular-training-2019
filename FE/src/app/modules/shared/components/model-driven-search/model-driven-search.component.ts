import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { SearchForm } from 'src/app/types/seatch';



@Component({
    selector: 'app-model-driven-search',
    template: `
        <form novalidate [formGroup]="form" (ngSubmit)="formSubmit()" class="text-center">
            <mat-form-field class="example-full-width">
                <input matInput
                    placeholder="Search..."
                    formControlName="query"
                    />
            </mat-form-field>
            <button
                mat-button
                color="primary"
                [disabled]="form.invalid"
                type="submit">cerca</button>
        </form>
    `,
    styles: [`
        mat-radio-button {
            margin-right: 15px
        }
    `]
})
export class ModelDrivenSearchComponent implements OnInit, OnDestroy {

    constructor(
        private fb: FormBuilder
    ) { }

    private subs: Subscription[] = [];
    @Input() searchTypes: string[][] = [];
    @Output() validSearch: EventEmitter<SearchForm> = new EventEmitter();

    // form: FormGroup = new FormGroup({
    //     query: new FormControl(''),
    //     type: new FormControl('planets', Validators.required),
    // });

    form: FormGroup = this.fb.group({
        query: ['', [Validators.required, Validators.pattern(/[0-9]/)]],
        // type: ['planets', Validators.required],
    });

    formSubmit() {
        if (this.form.value.query.length === 0) { return; }
        this.validSearch.emit(this.form.value);
    }

    ngOnInit() {
        // const sub = this.form.controls.type.valueChanges
        //     .pipe(
        //         tap( v => this.form.controls.query.setValue('')),
        //     ).subscribe(v => this.validSearch.emit(this.form.value));
        // this.subs.push(sub);
        // this.validSearch.emit(this.form.value);
    }

    ngOnDestroy(): void {
        this.subs.map( s => s.unsubscribe());
    }

}
