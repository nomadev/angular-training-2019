import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelDrivenSearchComponent } from './model-driven-search.component';

describe('ModelDrivenSearchComponent', () => {
  let component: ModelDrivenSearchComponent;
  let fixture: ComponentFixture<ModelDrivenSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelDrivenSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelDrivenSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
