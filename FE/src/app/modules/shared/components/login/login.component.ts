import { Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { CustomInput } from './../../../forms/form-field';
import { UserService } from './../../../../services/user.services';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(
        public userService: UserService,
        private dialog: MatDialog,
        private router: Router
    ) { }

    formModel = [
        new CustomInput({
            id: 'email',
            label: 'Email',
            type: 'email',
            placeholder: 'Inserisci la tua email',
            validators: [Validators.required]
        }),
        new CustomInput({
            id: 'password',
            label: 'Password',
            type: 'Inserisci la password',
            placeholder: '',
            validators: [Validators.required]
        }),
    ];

    ngOnInit() {
    }

    openLogin(loginTpl: TemplateRef<any>) {
        this.dialog.open(loginTpl);
    }

    login(form) {
        this.userService.login(form.email, form.password).subscribe(res => this.dialog.closeAll());
    }

    gotoSignup() {
        this.dialog.closeAll();
        this.router.navigateByUrl('/signup');
    }


}
