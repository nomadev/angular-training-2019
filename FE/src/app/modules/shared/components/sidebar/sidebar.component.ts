import { NavItem } from './../../../../types/nav-items';
import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';


export interface AppSidebarOptions {
    opened?: boolean;
    showToggle?: boolean;
}


@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnDestroy {
    mobileQuery: MediaQueryList;

    @Input() navItem: NavItem[] = [];
    @Input() options: AppSidebarOptions = {
        showToggle: true,
        opened: true
    };

    private mobileQueryListener: () => void;

    constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this.mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this.mobileQueryListener);
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this.mobileQueryListener);
    }


}
