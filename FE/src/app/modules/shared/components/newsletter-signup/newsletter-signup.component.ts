import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
    selector: 'app-newsletter-signup',
    template: `
        <form [formGroup]="form"></form>
    `,
  styleUrls: ['./newsletter-signup.component.scss']
})
export class NewsletterSignupComponent implements OnInit {

    constructor() { }

    form: FormGroup = new FormGroup({
        email: new FormControl('', [Validators.required])
    });

    ngOnInit() {
    }

}
