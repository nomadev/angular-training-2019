import { tap } from 'rxjs/operators';
import { Component, OnInit, Input, Output, EventEmitter, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { SearchForm, SearchType } from 'src/app/types/seatch';



@Component({
    selector: 'app-template-driven-search',
    template: `
        <form novalidate #form="ngForm" (ngSubmit)="formSubmit(form)" class="text-center">
            {{ form.valid | json }}<br />
            {{ form.value | json }}<br />
            <mat-form-field class="example-full-width">
                <input
                    #pippo="ngModel"
                    matInput
                    name="query"
                    ngModel
                    required
                    pattern="[A-Za-z]{1,3}"
                    placeholder="Search..."
                    />
            </mat-form-field>
            <span class="error" *ngIf="pippo.touched && pippo.invalid">campo invalido</span>
            {{ pippo?.valid }}
            <button
                mat-button
                color="primary"
                [disabled]="form.invalid"
                type="submit">cerca</button>
            <br />
            <!-- <mat-radio-group
                [(ngModel)]="model.type"
                (ngModelChange)="onTypeUpdate()"
                required
                name="type"
                aria-label="Select an option">
                <mat-radio-button
                    *ngFor="let type of searchTypes"
                    [value]="type[0]">{{type[1]}}</mat-radio-button>
            </mat-radio-group>
            -->
        </form>
    `,
    styles: [`
        mat-radio-button {
            margin-right: 15px
        }
    `]
})
export class TemplateDrivenSearchComponent implements OnInit, OnDestroy {

    constructor() { }


    @ViewChild('form', {static: false}) form;
    @Input() searchTypes: string[][] = [];
    @Output() validSearch: EventEmitter<SearchForm> = new EventEmitter();
    model = new SearchForm(SearchType.Planets, '');


    formSubmit() {
        if (this.form.invalid) { return; }
        this.validSearch.emit(this.model);
    }

    onTypeUpdate() {
        this.model.query = '';
        this.validSearch.emit(this.model);
    }

    ngOnInit() {
        // this.onTypeUpdate();
    }

    ngOnDestroy(): void {
    }

}
