import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateDrivenSearchComponent } from './template-driven-search.component';

describe('TemplateDrivenSearchComponent', () => {
  let component: TemplateDrivenSearchComponent;
  let fixture: ComponentFixture<TemplateDrivenSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplateDrivenSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateDrivenSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
