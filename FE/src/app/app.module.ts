import { CacheInterceptor } from './interceptor/cache.interceptor';
import { HttpResponseInterceptor } from './interceptor/http-response.interceptor';
import { SharedModule } from './modules/shared/shared.module';
import { SetProperHeadersService } from './interceptor/set-proper-headers.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        AppRoutingModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: CacheInterceptor,
            // deps: [],
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: SetProperHeadersService,
            // deps: [],
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpResponseInterceptor,
            multi: true,
        },
        Location, {provide: LocationStrategy, useClass: HashLocationStrategy},
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
