import { tap, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';


import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';


@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req instanceof HttpRequest) {
            return next.handle(req)
            .pipe(
                catchError(err => throwError(err)),
                tap((event: HttpEvent<any>) => {},
                    (error: any) => {
                        if (error instanceof HttpErrorResponse) {
                            console.error('errore!', error);
                            return throwError(error);
                        }
                    }
                ),
            );
        }
    }

}


