
import { Injectable } from '@angular/core';
import { HttpEvent, HttpResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { tap, shareReplay, catchError } from 'rxjs/operators';

import {
    HttpRequest,
    HttpHandler,
    HttpInterceptor
} from '@angular/common/http';
import { RequestCacheService } from '../services/request-cache.service';




@Injectable({
    providedIn: 'root'
})
export class CacheInterceptor implements HttpInterceptor {

    constructor(private cache: RequestCacheService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler) {

        /**
         * We cache only GET CALLS
         */
        if (req.method === 'GET') {
            const cachedResponse$ = this.cache.get(req.url);

            if (cachedResponse$ instanceof Observable)
                return cachedResponse$;
            else {
                const SIZE = 1;

                const xhrRequest$ = this.cacheRequest(req, next)
                                    /**
                                     * we need to share the same Observable, so that
                                     * many subscriptions will not trigger
                                     * many HttpRequests
                                     */
                                    .pipe(
                                        /**
                                         * This operator returns an Observable that shares a single
                                         * subscription to the underlying source, which is the Observable
                                         * from this XHR request
                                         */
                                        shareReplay(SIZE)
                                    );
                this.cache.cacheQueue.set(req.url, xhrRequest$);
                return xhrRequest$;
            }

        } else {
            return next.handle(req);
        }
    }

    cacheRequest(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(req)
            .pipe(
                tap(event => {
                    if (event instanceof HttpResponse) {
                        const TTL = 120;
                        this.cache.set(req.url, event, TTL);
                    }
                }),
                catchError(error => {
                    this.cache.delete(req.url);
                    return throwError(error);
                })
            );
    }
}
