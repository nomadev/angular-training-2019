import { Observable } from 'rxjs';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpHeaders
} from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SetProperHeadersService implements HttpInterceptor {

    private commonHeaders() {
        return {
            'Content-Type': 'application/json',
        };
    }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        if (req instanceof HttpRequest) {
            const headers = this.commonHeaders();
            const modifiedReq = req.clone({ headers: new HttpHeaders(headers) });
            return next.handle(modifiedReq);
        }
    }
}
