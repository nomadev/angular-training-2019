# AngularTraining

* Installare Node.js versione 8.x o 10.x.
* Da shell installare Angular Cli `npm install -g @angular/cli`
* Clonare o scaricare il repository
* Posizionarsi da shell sulla cartella del progetto ed installare le dipendenze tramite `npm install`
* Lanciare `ng serve` per lanciare un'istanza di Node per lo sviluppo
* Aprire il browser all'indirizzo `http://localhost:4200/`


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## TODO

Si deve realizzare un'applicazione Angular per i fan di Star Wars, interfacciata con le relative SWAPI (www.swapi.co).
La homepage dell'applicazione dovrà contenere una ricerca dove poter visualizzare le seguenti liste: 
    - Pianeti
    - Astronavi
    - Veicoli
    - Personaggi

Inoltre dovrà essere presente una pagina "dettaglio" dove poter vedere in dettaglio il risultato di ricerca selezionato
