"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const document = window.document;
class Utils {
    static append(content) {
        const node = document.createElement("LI");
        const textNode = document.createTextNode(content);
        node.appendChild(textNode);
        /**
         *  If you know from external means that an expression is not null or undefined,
         *  you can use the non-null assertion operator ! to coerce away those types:
         *
         * // Error, some.expr may be null or undefined
         * let x = some.expr.thing;
         * // OK
         * let y = some.expr!.thing;
         * */
        this._selector.appendChild(node);
    }
    static clear() {
        this._selector.innerHTML = '';
    }
}
Utils._selector = document.getElementById('content');
exports.Utils = Utils;
