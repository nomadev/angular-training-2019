"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
const utils_1 = require("./utils");
const a = rxjs_1.from(['World', 'peppe', 'zio']);
const b = rxjs_1.interval(1000).pipe(operators_1.take(2));
const source = rxjs_1.zip(a, b)
    .pipe(operators_1.map(x => x[0]), operators_1.tap((x) => console.log(x)), operators_1.tap((x) => utils_1.Utils.append(x)));
source.subscribe();
