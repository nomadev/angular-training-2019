import { p4 } from './scripts/p4.subjects';
import { p0 } from './scripts/p0.frp';
import { of, from, timer, concat, Observable, interval, zip } from 'rxjs';
import { map, tap, take, concatAll, combineAll, switchMap } from 'rxjs/operators';
import { Utils as u } from './utils';
import { p1 } from './scripts/p1.creazione';
import { p2 } from './scripts/p2.from';
import { p3 } from './scripts/p3.cold-hot';


// LINK A SLIDE RXJS
// https://www.slideshare.net/nucleode/reactive-programming-con-rxjs

// RXJS operators in depth
// https://blog.angularindepth.com/learn-to-combine-rxjs-sequences-with-super-intuitive-interactive-diagrams-20fce8e6511


p3  ();


// const a = from(['Ciao', 'a', 'tutti']);
// const b = interval(1000).pipe(take(2));
// const source = zip(a, b)
//     .pipe(
//         map(x => x[0]),
//         tap( (x: string) => console.log(x)),
//         tap( (x: string) => u.append(x))
//     )

// source.subscribe();
