
const document = window.document;

export class Utils {
    private static _selector = document.getElementById('content');
    static append(...content: any[]): void {
        const node = document.createElement("LI");
        const textNode = document.createTextNode(content.join(', '));
        node.appendChild(textNode);
        
        /**
         *  If you know from external means that an expression is not null or undefined, 
         *  you can use the non-null assertion operator ! to coerce away those types:
         * 
         * // Error, some.expr may be null or undefined
         * let x = some.expr.thing; 
         * // OK 
         * let y = some.expr!.thing;
         * */
        this._selector!.appendChild(node);
    }
    static clear(): void {
        this._selector!.innerHTML = '';
    }
}