/**
 * Functional Reactive Programming FRP è un paradigma di programmazione che permette di gestire un flusso di dati asincrono, 
 * servendosi dei capisaldi della programmazione funzionale (ad esempio dei metodi map, reduce, filter). 
 * La FRP è molto usata nella programmazione di interfacce grafiche, in robotica e in musica e ha 
 * come scopo quello di rendere più semplice la modelazione degli eventi che succedono nel tempo.
 */

/**
 * Imperative vs Declarative
 * 
 * Imperativo: 
 * Paradigma di programmazione secondo cui un programma viene inteso come un insieme di istruzioni, 
 * ciascuna delle quali può essere pensata come un "ordine" che viene impartito alla macchina virtuale 
 * del linguaggio di programmazione utilizzato. 
 * 
 * Dichiarativo:
 * Paradigma di programmazione che si focalizza sulla descrizione delle proprietà della soluzione desiderata (il cosa), 
 * lasciando indeterminato l'algoritmo da usare per trovare la soluzione (il come).
 */
// IMPERATIVO
export const p0 = () => {

    // IMPERATIVO
    const doubled: number[] = [];
    const doubleMap = (numbers: number[]) => {
        for (let i = 0; i < numbers.length; i++) {
            doubled.push(numbers[i] * 2)
        }
        return doubled;
    }
    console.log(doubleMap([2, 3, 4])) // [4, 6, 8]

    // DICHIARATIVO
    const doubleMap2 = (numbers: number[]) => numbers.map(n => n * 2); 
    console.log(doubleMap2([2, 3, 4])) // [4, 6, 8]

    /** Functional Programming è un paradigma di pogrammazione dichiarativo 
     *  il programma è l'esecuzione di una serie di funzioni usa tipi di dato che: 
     *      non sono mutabili 
     *      non cambia il valore di variabili definite fuori dallo scope in esecuzione (funzioni pure) 
     *      ogni chiamata succesiva a una stessa funzione, con gli stessi argomenti, produce lo stesso output
    */
   const source = ['1', '1', 'foo', '2', '3', '5', 'bar', '8', '13'];
   // applica la funzione parseInt a ogni elemento
   const result = source.map(x => parseInt(x)) 
                    // [1, 1, NaN, 2, 3, 5, NaN, 8, 13]
                    .filter(x => !isNaN(x))
                    // [1, 1, 2, 3, 5, 8, 13]
                    .reduce((x, y) => x + y); 
    console.log(result) // 33
}
