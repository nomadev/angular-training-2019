import { Observable } from 'rxjs';
import { Utils as u } from '../utils';

export const p1 = () => {

    /**
     * Observable: 
     *      oggetto (javascript) che possiede dei metodi (chiamati operatori) e delle proprietà 
     *      emettitore di eventi o valori in un certo lasso di tempo creato a partire da strutture "simili" (isomorfe) 
     */

    const obs1$ = Observable.create((observer: any) => {
        // throw new Error();
        observer.next('Hello World');
        observer.next('Hello Again!');
        observer.complete();
        observer.next('Bye');
    });
    
    obs1$.subscribe(
        (x:any) => u.append(x),
        (error: any) => u.append('Error: ', error),
        () => u.append('Completed')
    );




    const observer = {
        next: (x:any) => u.append(x),
        error: (error: any) => u.append('Error: ', error),
        complete: () => u.append('Completed')
    }
    const obs2$ = Observable.create( (observer: any) => observer.next(1));
    obs2$.subscribe(observer);








    var observable1 = Observable.create((observer: any) => {
        observer.next('Observable One is alive!');
        setInterval(() => {
            observer.next('Observable One');
        }, 5000);
    });

    var observable2 = Observable.create((observer: any) => {
        observer.next('Observable Two is alive!');
        setInterval(() => {
            observer.next('Observable Two');
        }, 2500);
    })

    // var subscription1 = observable1.subscribe(
    //     (x: any) => u.append(x, 1)
    // );

    // var subscription2 = observable2.subscribe(
    //     (x: any) => u.append(x, 2)
    // );



}
