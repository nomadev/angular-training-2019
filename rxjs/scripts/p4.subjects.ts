import { Observable, from, of, timer, interval, ConnectableObservable } from "rxjs";
import { Utils as u } from '../utils';
import { share, tap, mapTo, publish } from "rxjs/operators";
import { Subject } from 'rxjs';

export const p4 = () => {

    /**
     * What is a Subject? 
     * An RxJS Subject is a special type of Observable that allows values to be multicasted to many Observers. 
     * While plain Observables are unicast (each subscribed Observer owns an independent execution of the Observable), 
     * Subjects are multicast.
    */
   
 
   const subject = new Subject<number>();
    
   subject.subscribe({
     next: (v) => u.append(`observerA: ${v}`)
   });
   subject.subscribe({
     next: (v) => u.append(`observerB: ${v}`)
   });
    
   subject.next(1);
   subject.next(2);
    
}