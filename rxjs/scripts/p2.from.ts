import { Observable, from, of, timer, fromEvent } from "rxjs";
import { Utils as u } from '../utils';
import { share, tap, mapTo, throttleTime, debounceTime, scan, reduce } from "rxjs/operators";

export const p2 = () => {

    const source1 = from([1, 2, 3, 4, 5]);
    // source1.pipe(
    //     reduce((acc, val) => acc + val)
    // ).subscribe(r => u.append(r));
    source1.pipe(
        scan((acc, val) => acc + val))
    // .subscribe(r => u.append(r));
    // source1.subscribe(r => u.append(r));

    const source2 = from('Hello World');
    // source2.subscribe(r => u.append(r));
    // source2.pipe(scan((acc, val) => acc + val)).subscribe(r => u.append(r));
    // source2.pipe(reduce((acc, val) => acc + val)).subscribe(r => u.append(r));


    const promise = new Promise((resolve, reject) => { 
        setTimeout(() => {
            resolve('resolved!')
        }, 5000)
    });
    const fromPromise = from(promise);
    // fromPromise.toPromise().then(r => u.append(r))
    // const sub = fromPromise.subscribe(r => u.append(r));


    /**
     * FROM EVENTS
     */
    const clicks = fromEvent(document, 'click').pipe(tap(e => console.log(e)));
    // const sub = clicks.subscribe((e: any) => u.append(e.type, ' x:', e.x, ' y:', e.y));

    const el = document.getElementById('input')!;
    const keyUp = fromEvent(el, 'keyup');
    // keyUp.subscribe((e:any) => u.append(e.key));

    // Emit latest value when specified duration has passed.
    const throttleKeyUp = keyUp.pipe(throttleTime(500));
    // throttleKeyUp.subscribe((e:any) => u.append(e.key));

    // Discard emitted values that take less than the specified time between output
    const debouncedKeyUp = keyUp.pipe(
        debounceTime(500)
    );
    // debouncedKeyUp.subscribe((e:any) => u.append(e.key));

    /**
     * Operators - sono metodi che si applicano a uno stream
     * - possono essere concatenati se è necessario applicare più di un operatore 
     * - ogni operatore modifica lo stream dei valori emmessi dall'operatore che lo precede 
     * - ogni operatore produce un nuovo Observable (stream) 
     */
    /**
     * from observable like
     */
}
