import { Observable, from, of, timer, interval, ConnectableObservable } from "rxjs";
import { Utils as u } from '../utils';
import { share, tap, mapTo, publish } from "rxjs/operators";

export const p3 = () => {

    /**
     * HOT VS COLD COLD: 
     *  COLD: Observable che cominciano l'esecuzione dopo che viene invocato il metodo subscribe
     *  HOT: Observable che producono i valori anche prima che ci sia una subscription attiva in ascolto
     * 
     *  HOT: L' esecuzione è condivisa tra tutti gli observer. connect() // avvia l'esecuzione, è come chiamare .subscribe su un cold 
     */

    // const src$: ConnectableObservable<number> = interval(1000)
    //     .pipe(
    //         tap(r => u.append(r)),
    //         publish()
    //     ) as ConnectableObservable<number>;
    // src$.connect();

    
    // src$.subscribe(r => u.append('sub1', r));
    // src$.subscribe(r => u.append('sub2', r));

    const source = timer(1000);
    //log side effect, emit result
    const example = source.pipe(
        tap(() => u.append('***SIDE EFFECT***')),
        // mapTo('***RESULT***')
    );

    // example.subscribe( r => u.append('subscription1', r))
    // example.subscribe( r => u.append('subscription2', r))


    const sharedExample = example.pipe(share());

    sharedExample.subscribe( r => u.append('subscription1', r))
    sharedExample.subscribe( r => u.append('subscription2', r))

    // const obs1$ = Observable.create((observer: any) => {
    //     observer.next('obs1$ is alive: ' + Date.now());
    // }).pipe(
    //     // tap(v => u.append('obs1$ side effects')),
    //     share()
    // );

    // obs1$.subscribe((v: any) => u.append('subscription 1', v));
    // obs1$.subscribe((v: any) => u.append('subscription 2', v));


}